<?php

require_once("include/class/Sucursal.php");

$estados_mexico = array(
    1733 => 'Aguascalientes',
    1734 => 'Baja California',
    1735 => 'Baja California Sur',
    1736 => 'Campeche',
    1737 => 'Chiapas',
    1738 => 'Chihuahua',
    1739 => 'Coahuila de Zaragoza',
    1740 => 'Colima',
    1741 => 'Distrito Federal',
    1742 => 'Durango',
    1743 => 'Guanajuato',
    1744 => 'Guerrero',
    1745 => 'Hidalgo',
    1746 => 'Jalisco',
    1747 => 'México',
    1748 => 'Michoacán',
    1749 => 'Morelos',
    1750 => 'Nayarit',
    1751 => 'Nuevo León',
    1752 => 'Oaxaca',
    1753 => 'Puebla',
    1754 => 'Querétaro',
    1755 => 'Quintana Roo',
    1756 => 'San Luis Potosí',
    1757 => 'Sinaloa',
    1758 => 'Sonora',
    1759 => 'Tabasco',
    1760 => 'Tamaulipas',
    1761 => 'Tlaxcala',
    1762 => 'Veracruz',
    1763 => 'Yucatán',
    1764 => 'Zacatecas',
);


/* Para consultar Sucursales */
$sucursales_migrar = new Sucursal;
//$sucursales_migrar->probarConexiones();
//$personas_registradas = $oDatosPersonas->obtener_sucursales();


$sucursales = $sucursales_migrar->obtenerSucursales();
//var_dump($sucursales);
//var_dump($sucursales['resultado'][0]);
//$sucursal = $sucursales['resultado'][0];
foreach ($sucursales['resultado'] as $sucursal) {
    $sucursal_insertada = $sucursales_migrar->insertarPostWP(1, 'NOW()', 'NOW()', '', $sucursal['nombre'], '', 'publish', 'closed', 'closed', '', $sucursales_migrar->doSlug($sucursal['nombre']), 'sucursales');

    $post_sucursal = $sucursal_insertada['id'];

    $sucursales_migrar->insertarPostMetaWP($post_sucursal, 'sucursal_aparicion_post', 1);
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, '_sucursal_aparicion_post', 'field_sucursales_main_side_1');
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, 'sucursal_seleccion_estados', array_search($sucursal['estado'], $estados_mexico));
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, '_sucursal_seleccion_estados', 'field_sucursales_main_side_2');
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, 'sucursales_datos_llenado_direccion', $sucursal['direccion']);
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, '_sucursales_datos_llenado_direccion', 'field_sucursales_main_1');
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, 'sucursales_datos_llenado_telefono', $sucursal['telefono']);
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, '_sucursales_datos_llenado_telefono', 'field_sucursales_main_2');
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, 'sucursales_datos_llenado_email', $sucursal['email']);
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, '_sucursales_datos_llenado_email', 'field_sucursales_main_3');
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, 'sucursales_datos_llenado_nombre_gerente', $sucursal['gerente']);
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, '_sucursales_datos_llenado_nombre_gerente', 'field_sucursales_main_4');
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, 'sucursales_datos_llenado_horario', $sucursal['horarios']);
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, '_sucursales_datos_llenado_horario', 'field_sucursales_main_5');
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, 'sucursales_datos_llenado_imagen_alterna', '');
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, '_sucursales_datos_llenado_imagen_alterna', 'field_sucursales_main_6');



    $request_url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" . $sucursal['latitud'] . "," . $sucursal['longitud'] . "&sensor=true";

    $data = @file_get_contents($request_url); //Voy por el Json
    $jsondata = json_decode($data, true); //Lo descompongo para tratarlo

    $json_acf = 'a:3:{s:7:"address";s:' . strlen($jsondata['results'][0]['formatted_address']) . ':"' . $jsondata['results'][0]['formatted_address'] . '";s:3:"lat";s:' . strlen($sucursal['latitud']) . ':"' . $sucursal['latitud'] . '";s:3:"lng";s:' . strlen($sucursal['longitud']) . ':"' . $sucursal['longitud'] . '";}';


    $sucursales_migrar->insertarPostMetaWP($post_sucursal, 'sucursales_datos_llenado_mapa', trim($json_acf));
    $sucursales_migrar->insertarPostMetaWP($post_sucursal, '_sucursales_datos_llenado_mapa', 'field_sucursales_main_7');
    echo $post_sucursal."<br/>";
}




//print_r($personas_registradas);
 
/* Para registrar Personas */

//if($registro){ echo "Registro Satisfactorio"; }