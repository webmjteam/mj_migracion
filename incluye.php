<?php

require_once("include/class/Sucursal.php");

$url_base_iconos = "http://s3.mundojoven.com.s3.amazonaws.com/iconosTours/";
$url_base_imagen = "http://172.16.10.240/mundojoven_developer/wp-content/uploads/" . date("Y") . "/" . date("m") . "/";
$wp_upload_dir = "C:\\xampp\\htdocs\\mundojoven_developer/wp-content/uploads/" . date("Y") . "/" . date("m") . "/";


$iconos_migrar = new Sucursal;

$array_iconos = array(
    'Bus',
    'Camping',
    'Casa-huesped',
    'Comida',
    'Crucero',
    'Ferry',
    'Hotel',
    'Navegacion',
    'Surf',
    'Transporte-publico',
    'Tren',
    'Tren-nocturno',
    'Voluntariados',
    'Vuelo-internacional',
    'Vuelo-interno',
    'Rango-de-edad',
    'Todo-publico',
    'Hospedaje-compartido',
);
$ahora = date("Y-m-d H:i:s");
foreach($array_iconos as $icono){
    $nombre_titulo = strtoupper($iconos_migrar->stripAccents(str_replace("-","_",$icono)));
    $post_icono_insertado = $iconos_migrar->insertarPostWP(1, $ahora, $ahora, '', $nombre_titulo, '', 'publish', 'closed', 'closed', '', $iconos_migrar->doSlug($nombre_titulo), 'incluye');
    $icono_imagen = $iconos_migrar->insertarPostWP(1, $ahora, $ahora, '', $nombre_titulo, '', 'inherit', 'open', 'closed', '', $iconos_migrar->doNamePost($nombre_titulo), 'attachment', 'image/png', $post_icono_insertado['id'], $url_base_imagen . $nombre_titulo . ".png");
    
    //Ingreso las imagenes al POST META
    $array_image = array(
        "width" => 50,
        "height" => 50,
        "file" => date("Y") . "/" . date("m") . "/" . $nombre_titulo . ".png",
        "sizes" => array(),
        "image_meta" => array(
            "aperture" => 0,
            "credit" => "",
            "camera" => "",
            "caption" => "",
            "created_timestamp" => 0,
            "copyright" => "",
            "focal_length" => 0,
            "iso" => 0,
            "shutter_speed" => 0,
            "title" => "",
            "orientation" => 0,
            "keywords" => array()
        )
    );
    $imagen_seriada = serialize($array_image);
    //var_dump($imagen_seriada);
    //echo "<br/>";
    $iconos_migrar->insertarPostMetaWP($icono_imagen['id'], '_wp_attached_file', date("Y") . "/" . date("m") . "/" . $nombre_titulo . ".png");
    $iconos_migrar->insertarPostMetaWP($icono_imagen['id'], '_wp_attachment_metadata', $imagen_seriada);
    
    $iconos_migrar->insertarPostMetaWP($post_icono_insertado['id'], 'incluye_imagen_post', $icono_imagen['id']);
    $iconos_migrar->insertarPostMetaWP($post_icono_insertado['id'], '_incluye_imagen_post', 'field_posttype_incluye_index_1');
    
    $image_url = $url_base_iconos . $icono . ".png";
    $imagen_data = file_get_contents($image_url);
    $image_path = $wp_upload_dir . $nombre_titulo . ".png";
    file_put_contents($image_path, $imagen_data);
}

echo "DONE";
 

