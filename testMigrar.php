<?php

require_once("include/class/Sucursal.php");
$url_base_imagen = "http://dev2.mundojoven.com/wp-content/uploads/" . date("Y") . "/" . date("m") . "/";
$wp_upload_dir = "/mjv0/html/wp-content/uploads/" . date("Y") . "/" . date("m") . "/";
$place_holder_url = "http://s3.mundojoven.com.s3.amazonaws.com/placeholder/placeholder-750x340.jpg";

$tours_migrar = new Sucursal;

//1. Leo la tabla de tours
$array_tours = $tours_migrar->obtenerToursSabana();
//var_dump($array_tours['resultado'][0]);
foreach ($array_tours['resultado'] as $tour) {
//$tour = $array_tours['resultado'][0];



    echo date("Y-m-d H:i:s");
    echo "<br/>";
    echo "<br/>";



//Inserto un Post de Tour

    $tours_insertado = $tours_migrar->insertarPostWP(1, date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), $tour['descripcion'], $tour['titulo'], '', 'publish', 'closed', 'closed', '', $tours_migrar->doSlug($tour['titulo']), 'tours');
    $id_post_final = $tours_insertado['id'];
    var_dump($id_post_final);
//$post_tour_new = $tours_insertado['id'];
//Leo de la tabla de la sabana de imagenes las 3 url
    $tour_carro_imagen = array();
    $imagenes_array = $tours_migrar->obtenerImagenesUrl((int) $tour['id']);

    if (isset($imagenes_array)) {
        $tour_carro_imagen[0] = $imagenes_array['resultado'][0]['primera'];
        $tour_carro_imagen[1] = $imagenes_array['resultado'][0]['segunda'];
        $tour_carro_imagen[2] = $imagenes_array['resultado'][0]['tercera'];
    }


//$slug_tour = $tours_migrar->doSlug($tour['titulo']);
//var_dump($tour_carro_imagen);
//Inserto 3 post con las imagenes

    foreach ($tour_carro_imagen as $i => $imagen) {

        $nombre_imagen = $tours_migrar->doNamePost($tour['titulo']) . '_' . $i . '.jpg';
        $post_imagen = $tours_migrar->insertarPostWP(1, date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), '', $tours_migrar->doNamePost($tour['titulo']), '', 'inherit', 'open', 'closed', '', $tours_migrar->doNamePost($tour['titulo']), 'attachment', 'image/jpeg', $tours_insertado['id'], $url_base_imagen . $nombre_imagen);

        $nombre_imagen = $tours_migrar->doNamePost($tour['titulo']) . '_' . $i . '.jpg';
        var_dump($nombre_imagen);
        echo "<br/>";
        $image_url = $imagen;


        $imagen_data = file_get_contents($image_url);
        list($width, $height) = getimagesize($image_url);
        var_dump($width);
        if ($width < 750) {
            $imagen_data_f = file_get_contents($place_holder_url);
        } else {
            $imagen_data_f = $imagen_data;
        }


        $image_path = $wp_upload_dir . $nombre_imagen;
        //Guardo la imagen
        file_put_contents($image_path, $imagen_data_f);
        //Ingreso las imagenes al POST META
        $array_image = array(
            "width" => 750,
            "height" => 340,
            "file" => date("Y") . "/" . date("m") . "/" . $nombre_imagen,
            "sizes" => array(),
            "image_meta" => array(
                "aperture" => 0,
                "credit" => "",
                "camera" => "",
                "caption" => "",
                "created_timestamp" => 0,
                "copyright" => "",
                "focal_length" => 0,
                "iso" => 0,
                "shutter_speed" => 0,
                "title" => "",
                "orientation" => 0,
                "keywords" => array()
            )
        );
        $imagen_seriada = serialize($array_image);
        //var_dump($imagen_seriada);
        //echo "<br/>";
        $tours_migrar->insertarPostMetaWP($post_imagen['id'], '_wp_attachment_metadata', $imagen_seriada);
        $tours_migrar->insertarPostMetaWP($post_imagen['id'], '_wp_attached_file', date("Y") . "/" . date("m") . "/" . $nombre_imagen);


        $ids_imagenes_insertadas[$i] = $post_imagen['id'];
    }

    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tour_aparece_carousel_home', 0);
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tour_aparece_carousel_home', 'field_aparece_tours_1');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tour_aparece_viajessugeridos', 0);
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tour_aparece_viajessugeridos', 'field_aparece_tours_2');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tour_aparece_homegaleria', 0);
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tour_aparece_homegaleria', 'field_aparece_tours_3');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'subtitulo', '');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_subtitulo', 'field_tour_basica_0');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'desde', 'desde');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_desde', 'field_tour_basica_1');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'precio_signo', 'peso');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_precio_signo', 'field_tour_basica_2');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'precio', (float) $tour['precio']);
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_precio', 'field_tour_basica_3');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'precio_moneda', $tour['moneda']);
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_precio_moneda', 'field_tour_basica_4');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'boton', 'Ver Más');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_boton', 'field_tour_basica_5');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'imagen_carousel_principal', '');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_imagen_carousel_principal', 'field_tour_imagen_1');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'imagen_carousel_principal_retina', '');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_imagen_carousel_principal_retina', 'field_tour_imagen_2');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'imagen_viajessugeridos_galeriahome', '');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_imagen_viajessugeridos_galeriahome', 'field_tour_imagen_3');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'imagenes_tour_single_repeater_0_imagen_tour_single_item', (int) $ids_imagenes_insertadas[0]);
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_imagenes_tour_single_repeater_0_imagen_tour_single_item', 'imagen_tour_single_item');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'imagenes_tour_single_repeater_1_imagen_tour_single_item', (int) $ids_imagenes_insertadas[1]);
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_imagenes_tour_single_repeater_1_imagen_tour_single_item', 'imagen_tour_single_item');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'imagenes_tour_single_repeater_2_imagen_tour_single_item', (int) $ids_imagenes_insertadas[2]);
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_imagenes_tour_single_repeater_2_imagen_tour_single_item', 'imagen_tour_single_item');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'imagenes_tour_single_repeater', 3);
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_imagenes_tour_single_repeater', 'field_tour_imagen_4');

    $idioma = '';
    if ($tour['idioma'] == "Español") {
        $idioma = 'a:1:{i:0;s:7:"espanol";}';
    }
    if ($tour['idioma'] == "Ingles") {
        $idioma = 'a:1:{i:0;s:6:"ingles";}';
    }

    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tours_single_idioma', $idioma);
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tours_single_idioma', 'field_tour_single_1');

    $array_incluye = $tour['incluye'];
    $array_incluye_items = explode(',', trim($array_incluye, ','));
    foreach ($array_incluye_items as $i_incluye => $item_incluye) {
        $post_id_imagen = $tours_migrar->obtenerPostId(trim($item_incluye), 'incluye');
        $array_incluye_f[$i_incluye] = (int) $post_id_imagen['resultado'][0]['ID'];
    }
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tours_single_iconos_incluye', serialize($array_incluye_f));
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tours_single_iconos_incluye', 'field_tour_single_5');

//Saco la info de la sabana
    $post_incluye_sabana = $tours_migrar->obtenerIncluyeSabana((int) $tour['id']);
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'contenido_incluye_inclusiones', $post_incluye_sabana['resultado'][0]['C1']); //TODO:Falta el la info 
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_contenido_incluye_inclusiones', 'field_tour_single_6'); //TODO:Falta el la info 

    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'contenido_no_incluye_inclusiones', $post_incluye_sabana['resultado'][0]['C2']);
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_contenido_no_incluye_inclusiones', 'field_tour_single_7');

    $categoria = $tour['categoria'];
    $post_categoria = $tours_migrar->obtenerPostId(trim($categoria), 'tipo_tours');
    var_dump($categoria);
    echo "<br/>";
    var_dump($post_categoria);
    echo "<br/>";
    echo "<br/>";
    if ($post_categoria['resultado']) {

        $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tours_avanzado_tipo', (int) $post_categoria['resultado'][0]['ID']);
    } else {
        $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tours_avanzado_tipo', '');
    }
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tours_avanzado_tipo', 'field_tour_single_avanzado_1');

    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tours_avanzado_proveedor', '');
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tours_avanzado_proveedor', 'field_tour_single_avanzado_2');

    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tour_itinerario_numerodias', (int) $tour['dias']);
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tour_itinerario_numerodias', 'field_itinerario_1');

    $array_ciudades = explode(',', trim($tour['ciudades'], ','));
//var_dump($array_ciudades);
    foreach ($array_ciudades as $i_ciudad => $ciudad) {
        $latlong = $tours_migrar->Get_LatLng_From_Google_Maps($ciudad);
        //var_dump($latlong);

        $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tour_itinerario_ciudades_repeater_' . $i_ciudad . '_tour_ciudades_nombre', $ciudad);
        $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tour_itinerario_ciudades_repeater_' . $i_ciudad . '_tour_ciudades_nombre', 'field_itinerario_ciudades_repeater_1');
    }
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tour_itinerario_ciudades_repeater', count($array_ciudades));
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tour_itinerario_ciudades_repeater', 'field_itinerario_ciudades_repeater');

    foreach ($array_ciudades as $i_ciudad => $ciudad) {
        $latlong = $tours_migrar->Get_LatLng_From_Google_Maps($ciudad);

        $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tour_itinerario_evento_repeater_' . $i_ciudad . '_tour_itinerario_item_titulo', $ciudad);
        $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tour_itinerario_evento_repeater_' . $i_ciudad . '_tour_itinerario_item_titulo', 'field_itinerario_evento_repeater_1');

        $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tour_itinerario_evento_repeater_' . $i_ciudad . '_tour_itinerario_icono', 'start');
        $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tour_itinerario_evento_repeater_' . $i_ciudad . '_tour_itinerario_icono', 'field_itinerario_evento_repeater_11');

        $contenido_itinerario = '';
        if ($i_ciudad == 0) {
            $contenido_itinerario = $tour['itinerario'];
        }

        $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tour_itinerario_evento_repeater_' . $i_ciudad . '_tour_itinerario_contenido', $ciudad . $contenido_itinerario);
        $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tour_itinerario_evento_repeater_' . $i_ciudad . '_tour_itinerario_contenido', 'field_itinerario_evento_repeater_2');

        $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tour_itinerario_evento_repeater_' . $i_ciudad . '_tour_itinerario_item_mapa', serialize($latlong));
        $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tour_itinerario_evento_repeater_' . $i_ciudad . '_tour_itinerario_item_mapa', 'field_itinerario_evento_repeater_3');
    }
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], 'tour_itinerario_evento_repeater', count($array_ciudades));
    $tours_migrar->insertarPostMetaWP($tours_insertado['id'], '_tour_itinerario_evento_repeater', 'field_itinerario_evento_repeater');


    /*

      $tours_migrar->insertarPostMetaWP($tour['id'], '', );
      $tours_migrar->insertarPostMetaWP($tour['id'], '', '');


      tours_single_region_mexico	a:3:{i:0;s:4:"1733";i:1;s:4:"1735";i:2;s:4:"1736";}
      _tours_single_region_mexico	field_tour_single__mexico_1
      tours_single_region	a:2:{i:0;s:1:"1";i:1;s:1:"4";}
      _tours_single_region	field_tour_single_2
      tours_single_pais	a:3:{i:0;s:3:"144";i:1;s:3:"114";i:2;s:2:"18";}
      _tours_single_pais	field_tour_single_3

      tour_promocion_nuevoprecio	1600
      _tour_promocion_nuevoprecio	field_tour_promocion_0
      tour_promocion_promocionotro	MIGRACION_PROMO
      _tour_promocion_promocionotro	field_tour_promocion_1
      tour_promocion_vigencia_inicio	1453483260
      _tour_promocion_vigencia_inicio	field_tour_promocion_3
      tour_promocion_vigencia_fin	1454198400
      _tour_promocion_vigencia_fin	field_tour_promocion_4

     */

    echo "done";
}





