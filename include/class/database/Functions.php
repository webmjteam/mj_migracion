<?php

//require("Config.php");

class conectorDB extends configuracion //clase principal de conexion y consultas { {
{
    private $conexion;

    public function __construct($nombre_base = null) {
        if ($nombre_base == null)
            $this->conexion = parent::conectar(); //creo una variable con la conexión
        if ($nombre_base == 'wordpress')
            $this->conexion = parent::conectarWordpress();

        return $this->conexion;
    }

    public function consultarBD($consulta, $valores = array()) {  //funcion principal, ejecuta todas las consultas
        $resultado = false;

        if ($statement = $this->conexion->prepare($consulta)) {  //prepara la consulta
            if (preg_match_all("/(:\w+)/", $consulta, $campo, PREG_PATTERN_ORDER)) { //tomo los nombres de los campos iniciados con :xxxxx
                $campo = array_pop($campo); //inserto en un arreglo
                foreach ($campo as $parametro) {
                    $statement->bindValue($parametro, $valores[substr($parametro, 1)]);
                }
            }
            //var_dump($consulta);
            try {
                if (!$statement->execute()) { //si no se ejecuta la consulta...
                    print_r($statement->errorInfo()); //imprimir errores
                }
                $resultado_temp = $statement->fetchAll(PDO::FETCH_ASSOC); //si es una consulta que devuelve valores los guarda en un arreglo.
                $last_id = $this->conexion->lastInsertId('ID');
                $resultado = array('resultado'=> $resultado_temp,'id'=>$last_id);
                $statement->closeCursor();
            } catch (PDOException $e) {
                echo "Error de ejecución: \n";
                print_r($e->getMessage());
            }
        }
        return $resultado;
        $this->conexion = null; //cerramos la conexión
    }

    

}

abstract class configuracion {

    protected $datahost;

    protected function conectar($archivo = 'database.ini') {

        if (!$ajustes = parse_ini_file($archivo, true))
            throw new exception('No se puede abrir el archivo ' . $archivo . '.');
        $controlador = $ajustes["database"]["driver"]; //controlador (MySQL la mayoría de las veces)
        $servidor = $ajustes["database"]["host"]; //servidor como localhost o 127.0.0.1 usar este ultimo cuando el puerto sea diferente
        $puerto = $ajustes["database"]["port"]; //Puerto de la BD
        $basedatos = $ajustes["database"]["schema"]; //nombre de la base de datos

        try {
            return $this->datahost = new PDO(
                    "mysql:host=$servidor;port=$puerto;dbname=$basedatos", $ajustes['database']['username'], //usuario
                    $ajustes['database']['password'], //constrasena
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
            );
        } catch (PDOException $e) {
            echo "Error en la conexión: " . $e->getMessage();
        }
    }

    protected function conectarWordpress($archivo = 'wordpress.ini') {

        if (!$ajustes = parse_ini_file($archivo, true))
            throw new exception('No se puede abrir el archivo ' . $archivo . '.');
        $controlador = $ajustes["database"]["driver"]; //controlador (MySQL la mayoría de las veces)
        $servidor = $ajustes["database"]["host"]; //servidor como localhost o 127.0.0.1 usar este ultimo cuando el puerto sea diferente
        $puerto = $ajustes["database"]["port"]; //Puerto de la BD
        $basedatos = $ajustes["database"]["schema"]; //nombre de la base de datos

        try {
            return $this->datahost = new PDO(
                    "mysql:host=$servidor;port=$puerto;dbname=$basedatos", $ajustes['database']['username'], //usuario
                    $ajustes['database']['password'], //constrasena
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
            );
        } catch (PDOException $e) {
            echo "Error en la conexión: " . $e->getMessage();
        }
    }

}
