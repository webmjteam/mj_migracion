<?php

require('database/Functions.php');

class Sucursal {

    private $sucursal;

    public function insertarPostMetaWP($post_id, $meta_key, $meta_value) {
        $wpConectar = new conectorDB('wordpress');
        $consulta = "INSERT INTO wp_postmeta (post_id, meta_key, meta_value)
                    VALUES (:post_id, :meta_key, :meta_value);
                ";
        $valores = array(
            'post_id' => $post_id,
            'meta_key' => $meta_key,
            'meta_value' => $meta_value
        );

        $registrar = $wpConectar->consultarBD($consulta, $valores);
        if ($registrar !== false) {
            return $registrar;
        } else {
            return false;
        }
    }

    public function insertarPostWP($post_author, $post_date, $post_date_gmt, $post_content, $post_title, $post_excerpt, $post_status, $comment_status, $ping_status, $post_password, $post_name, $post_type, $post_mime_type = '', $post_parent = '', $guid = '') {
        $wpConectar = new conectorDB('wordpress');
        $consulta = "INSERT INTO wp_posts (post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, post_type, post_mime_type,post_parent, guid)
                    VALUES (:post_author, :post_date, :post_date_gmt, :post_content, :post_title, :post_excerpt, :post_status, :comment_status, :ping_status, :post_password, :post_name, :post_type, :post_mime_type, :post_parent, :guid)";

        //VALORES PARA REGISTRO
        $valores = array(
            'post_author' => $post_author,
            'post_date' => $post_date,
            'post_date_gmt' => $post_date_gmt,
            'post_content' => $post_content,
            'post_title' => $post_title,
            'post_excerpt' => $post_excerpt,
            'post_status' => $post_status,
            'comment_status' => $comment_status,
            'ping_status' => $ping_status,
            'post_password' => $post_password,
            'post_name' => $post_name,
            'post_type' => $post_type,
            'post_mime_type' => $post_mime_type,
            'post_parent' => $post_parent,
            'guid' => $guid,
        );
        //var_dump($consulta);
        //$nombre,$direccion,$telefono,$email, $gerente, $horarios

        $registrar = $wpConectar->consultarBD($consulta, $valores);

        if ($registrar !== false) {
            return $registrar;
        } else {
            return false;
        }
    }

    public function probarConexiones() {
        $oConectar = new conectorDB; //instanciamos conector
        $wordpressConectar = new conectorDB('wordpress'); //instanciamos conector

        $consulta = "SELECT * FROM filtrado_sucursales";
        $consultaWP = "SELECT * FROM wp_users";

        $resultado = $oConectar->consultarBD($consulta, array());
        $resultadoWP = $wordpressConectar->consultarBD($consultaWP, array());

        var_dump($resultado);
        echo "<br/>";
        echo "<br/>";
        var_dump($resultadoWP);
    }

    public function obtenerSucursales() {
        $consulta = "SELECT * FROM filtrado_sucursales";
        $valores = null;

        $oConectar = new conectorDB; //instanciamos conector



        $this->sucursal = $oConectar->consultarBD($consulta, $valores);

        return $this->sucursal;
    }

    public function obtenerToursSabana() {
        $consulta = "SELECT * FROM filtrado_tours";
        $valores = null;

        $oConectar = new conectorDB; //instanciamos conector



        $this->sucursal = $oConectar->consultarBD($consulta, $valores);

        return $this->sucursal;
    }

    public function obtenerImagenesUrl($post_id) {
        $consulta = 'SELECT * FROM sabana_imagenes WHERE id="' . $post_id . '"';
        $valores = null;

        $oConectar = new conectorDB; //instanciamos conector



        $this->sucursal = $oConectar->consultarBD($consulta, $valores);

        return $this->sucursal;
    }

    public function obtenerPostId($nombre_post = "", $post_type = "") {
        $consulta = 'SELECT * FROM wp_posts WHERE post_title="' . $nombre_post . '" AND post_type="' . $post_type . '"';
        $valores = null;
        //var_dump($consulta);
        $oConectar = new conectorDB('wordpress'); //instanciamos conector



        $this->sucursal = $oConectar->consultarBD($consulta, $valores);

        return $this->sucursal;
    }

    function check_status_json($jsondata) {
        if ($jsondata["status"] == "OK")
            return true;
        return false;
    }

    function Get_LatLng_From_Google_Maps($address) {
        $address = urlencode($address);

        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false";

        //Voy por el Json
        $data = @file_get_contents($url);
        //Lo descompongo para tratarlo
        $jsondata = json_decode($data, true);

        //Verifico si encontro el lugar
        if (!$this->check_status_json($jsondata))
            return array();

        $LatLng = array(
            'address' => $jsondata["results"][0]["address_components"][0]["long_name"],
            'lat' => $jsondata["results"][0]["geometry"]["location"]["lat"],
            'lng' => $jsondata["results"][0]["geometry"]["location"]["lng"],
            
        );

        return $LatLng;
    }

//Termina funcion obtenerPersonas();

    /*
      public function newSucursalWordpress($nombre,$direccion,$telefono,$email,$gerente,$horarios){
      $registrar = false; //creamos una variable de control
      $consulta = "INSERT INTO
      VALUES (:nombre, :apellido, :edad)";

      //VALORES PARA REGISTRO
      $valores = array("nombre"=>$nombre,
      "apellido"=>$apellido,
      "edad"=>$edad);

      $oConexion = new conectorDB; //instanciamos conector
      $registrar = $oConexion->consultarBD($consulta, $valores);

      if($registrar !== false){
      return true;
      }
      else{
      return false;
      }
      } //Termina funcion registrarUsuarios()
     * 
     */

    public function stripAccents($cadena) {
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = strtolower($cadena);
        return utf8_encode($cadena);
    }

    function doSlug($string) {
        return strtolower(str_replace("&", "and", str_replace(" ", "-", $this->stripAccents($string))));
    }

    function doNamePost($string) {
        return strtolower(str_replace("&", "and", str_replace(" ", "_", $this->stripAccents($string))));
    }

}
